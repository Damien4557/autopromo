USE AutoPromo;

CREATE TABLE voiture
(
    id                       INT                NOT NULL AUTO_INCREMENT,
    nom_voiture              VARCHAR(100)       NOT NULL,
    description_voiture      TEXT,
    image_voiture            VARCHAR(250),
    prix_voiture             DECIMAL,
    couleur_voiture          VARCHAR(100),
    etat_voiture             VARCHAR(100),
    primary key (id)
);
