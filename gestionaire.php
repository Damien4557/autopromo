<?php
include "config.php";
$model = isset($_POST['model']) ? $_POST['model'] : '';
$color = isset($_POST['color']) ? $_POST['color'] : '';
$etat =  isset($_POST['etat']) ? $_POST['etat'] : '';
$price =  isset($_POST['price']) ? $_POST['price'] : '';
$description =  isset($_POST['description']) ? $_POST['description'] : '';
$image_file = isset($_POST['image_film']) ? $_POST['image_film'] : 0;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="gestionaire.php" method="POST">
        <label for="model">Nom voiture</label>
        <select class="input_search" name="model" id="model">
            <option value="modelY">Tesla Model Y</option>
            <option value="modelS">Tesla Model S</option>
            <option value="modelX">Tesla Model X</option>
            <option value="model3">Tesla Model 3</option>
        </select>
        <label for="color">couleur voiture</label>

        <select class="input_search" name="color" id="color">
            <option value="noir">Noir</option>
            <option value="bleu">bleu</option>
            <option value="rouge">rouge</option>
            <option value="blanche">blanche</option>
        </select>
        <label for="description">description voiture</label>
        <textarea name="description" id="" cols="30" rows="10"></textarea>
        <label for="price">prix</label>
        <input type="number" name="price" id="">
        <label for="etat">etat voiture</label>
        <select class="input_search" name="etat" id="etat">
            <option value="neuve">Neuve</option>
            <option value="occasion">Occasion</option>
        </select>
        <label for="img">image de la voiture</label>
        <input type="file" name="image_film" id="">
        <button class="input_search" type="submit">
            <ion-icon class="icon" name="search"></ion-icon>envoyer
        </button>
    </form>

    <?php
    if ($_FILES['image_film']) {
        $image_file = "img/". basename($_FILES['image_film']['name']);
    }
    try {

        if ($model != '' && $color != '' && $image_file != '' && $etat != '') {
            $insert = $bdd->prepare("INSERT INTO `voiture`(`id`, `nom_voiture`, `description_voiture`, `image_voiture`,`prix_voiture`,`couleur_voiture`,`etat_voiture`) VALUES (NULL,:nom,:descriptions,:img,:price,:color,:etat)");
            $insert->execute(['nom' => $model, 'descriptions' => $description, 'img' => $image_file, 'price' => $price, 'color' => $color, 'etat' => $etat]);
        }
    } catch (PDOException $e) {
        echo "erreur a la connexion : " . $e->getMessage();
    }

    ?>
</body>

</html>