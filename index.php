<?php
include "config.php";
$model = isset($_POST['model']) ? $_POST['model'] : '';
$color = isset($_POST['color']) ? $_POST['color'] : '';
$etat =  isset($_POST['etat']) ? $_POST['etat'] : '';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <main class="container">
        <nav class="navbar">
            <a href="index.php"><span style="display: flex;align-items:center"><img src="img/Tesla-logo.jpg" width="50px" alt="">
                    <h1>Auto-Promo</h1>
                </span></a>
            <ul class="menu">
                <a href="index.php">
                    <li>Nos voitures</li>
                </a>
                <li>A propos de nous</li>
                <a href="contact.php">
                    <li>Contact</li>
                </a>
            </ul>
        </nav>
        <article class="conatiner_choce">
            <form action="index.php" method="POST">
                <select class="input_search" name="model" id="model">
                    <option value="tout_model">Toute</option>
                    <option value="modelY">Tesla Model Y</option>
                    <option value="modelS">Tesla Model S</option>
                    <option value="modelX">Tesla Model X</option>
                    <option value="model3">Tesla Model 3</option>
                </select>
                <select class="input_search" name="color" id="color">
                    <option value="toute_color">Toute</option>
                    <option value="noir">Noir</option>
                    <option value="bleu">bleu</option>
                    <option value="rouge">rouge</option>
                    <option value="blanche">blanche</option>
                </select>
                <select class="input_search" name="etat" id="etat">
                    <option value="tous">Les deux</option>
                    <option value="neuve">Neuve</option>
                    <option value="occ">Occasion</option>
                </select>
                <button class="input_search" type="submit">
                    <ion-icon class="icon" name="search"></ion-icon>
                </button>
            </form>
            <?php

            // $sql_search = $bdd->prepare("SELECT * FROM voiture WHERE nom_voiture = $model");
            // $sql_search->execute();
            // $result = $sql_search->fetchALL();

            // foreach ($result as $key) {
            //     echo '<div class="card_voiture"><img class="imgProduit" src="img/' . $key['image_voiture'] . '" alt="img du produit">
            //     <div class="info_voiture">
            //     <div><h3>' . $key['nom_voiture'] . '</h3><p>' . $key['prix_voiture'] . '</p></div>
            //     <p>' . $key['couleur_voiture'] . '</p>
            //     <div><p>' . $key['description_voiture'] . '</p>
            // <a class="affiche" href="">voir</a></div>
            // </div>
            // </div>';
            // }


            ?>
        </article>
        <article class="container_etat">
            <div class="card_etat">
                <div class="information">
                    <h2>Véhicules d'occasion</h2>
                    <a class="affiche" href='index.php?id'>voir</a>
                </div>
            </div>
            <div class="card_etat">
                <div class="information">
                    <h2>Véhicules neufs</h2>
                    <a class="affiche" href="">voir</a>
                </div>
            </div>
            <div class="card_etat">
                <div class="information">
                    <h2>Rachat de votre véhicule</h2>
                    <a class="affiche" href="">voir</a>
                </div>
            </div>
        </article>
        <article class="container_produit">
            <div class="card_produit"><img src="" alt=""></div>

        </article>
        <div class="fonctionnement">
            <h2>Auto-Promo, <span>comment ca marche ?</span></h2>
            <ul>
                <li>
                    <div class="etape"><img width="100%" height="100%" src="svg/how-it-works-1-recherche-vehicule.svg" alt="" srcset="">
                        <h3>Recherche du <br> véhicule</h3>
                        <p>Décrivez votre projet en indiquant le véhicule que vous recherchez et la zone où vous vous trouvez.</p>
                    </div>
                </li>
                <li>
                    <div class="etape"><img width="100%" height="100%" src="svg/how-it-works-2-affichage-bonnes-affaires.svg" alt="" srcset="">
                        <h3>Affichage de nos bonne affaires</h3>
                        <p>Nous affichons tous les véhicules disponibles chez nos concessionnaires et proches de chez vous.</p>
                    </div>
                </li>
                <li>
                    <div class="etape"><img width="100%" height="100%" src="svg/how-it-works-3-reservation-vehicule-choisi.svg" alt="" srcset="">
                        <h3>Reservation du <br> vehicule</h3>
                        <p>Contactez-nous, un conseiller transmettra votre demande à une concession.</p>
                    </div>
                </li>
                <li>
                    <div class="etape"><img width="100%" height="120px" src="svg/how-it-works-4-rendez-vous-concession.svg" alt="" srcset="">
                        <h3>Prise de rendez-vous avec le concessionnaire</h3>
                        <p>Un concessionnaire vous appelle pour fixer un rendez-vous, selon vos disponibilités.</p>
                    </div>
                </li>
                <li>
                    <div class="etape"><img width="100%" height="100%" src="svg/how-it-works-5-achat-vehicule.svg" alt="" srcset="">
                        <h3>Achat du <br> vehicule</h3>
                        <p>Le concessionnaire vous présente le véhicule et vous réalisez directement votre achat en concession.</p>
                    </div>
                </li>
            </ul>
        </div>
        <h2 style="text-align: center;">Véhicules qui pourraient <span> vous intéresser ?</span></h2>
        <article class="container_proposition">
            <?php

            $sql = $bdd->prepare("SELECT * FROM voiture ORDER BY RAND() LIMIT 10");
            $sql->execute();
            $result = $sql->fetchALL();

            foreach ($result as $key) {
                echo '<div class="card_voiture"><img class="imgProduit" src="img/' . $key['image_voiture'] . '" alt="img du produit">
                <div class="info_voiture">
                <div><h3>' . $key['nom_voiture'] . '</h3><p>' . $key['prix_voiture'] . '</p></div>
                <p>' . $key['couleur_voiture'] . '</p>
                <div><p>' . $key['description_voiture'] . '</p>
            <a class="affiche" href="">voir</a></div>
            </div>
            </div>';
            }
            ?>

        </article>

        <article class="container_partenaire">
            <a href="gestionaire.php">coin gestion </a>
        </article>
    </main>
</body>

</html>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>