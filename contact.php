<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div id="toast"></div>
    <div id="up" style="height: 10vh"></div>
    <main class="container">
        <nav class="navbar">
            <a href="index.php"><span style="display: flex;align-items:center"><img src="img/Tesla-logo.jpg" width="50px" alt="">
                <h1>Auto-Promo</h1>
            </span></a>
            <ul class="menu">
                <a href="index.php">
                    <li>Nos voitures</li>
                </a>

                <li>A propos de nous</li>
                <a href="contact.php">
                    <li>Contact</li>
                </a>
            </ul>
        </nav>
        <h2 class="titre">Contact</h2>
        <article class="container_contact">
            <form action="index.php">
                <div id="bloc_nom_prenom">
                    <div> <label for="">Nom</label>
                        <input class="input" placeholder="Votre Nom" type="text">
                    </div>
                    <div> <label for="">Prenom</label>
                        <input class="input" placeholder="Votre Prenom" type="text">
                    </div>
                </div>

                <label for="">Email</label>

                <input class="input" placeholder="Votre Email" type="email" pattern="+@\.com" size="30">
                <label for="">Commentaire</label>

                <textarea class="input" type="text" placeholder="Votre Message"></textarea>



            </form>
            <button onclick="wishList()" class="btn_message" type="submit">Envoyer</button>
        </article>
    </main>
</body>

</html>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script>
    function wishList() {
        var list = document.getElementById("toast");
        list.classList.add("show");
        list.innerHTML = '<ion-icon class="icon_message" name="chatbubble"></ion-icon></ion-icon> Message envoyé';
        setTimeout(function() {
            list.classList.remove("show");
        }, 3000);
    }
</script>